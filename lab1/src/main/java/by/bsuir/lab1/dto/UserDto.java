package by.bsuir.lab1.dto;

import lombok.Data;

@Data
public class UserDto {
    private Long id;

    private String surname;

    private String name;

    private String patronymic;

    private String phone;

    private String passportNumber;
}
