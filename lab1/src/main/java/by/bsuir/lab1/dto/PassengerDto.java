package by.bsuir.lab1.dto;

import lombok.Data;

@Data
public class PassengerDto {

    Long id;

    UserDto userDto;

    RouteDto routeDto;

}
