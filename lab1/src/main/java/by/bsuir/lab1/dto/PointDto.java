package by.bsuir.lab1.dto;

import lombok.Data;

@Data
public class PointDto {

    private Long id;

    private String name;
}
