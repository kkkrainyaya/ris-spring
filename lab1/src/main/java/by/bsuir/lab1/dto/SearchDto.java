package by.bsuir.lab1.dto;

import by.bsuir.lab1.entity.Point;
import lombok.Data;

import java.time.LocalDate;

@Data
public class SearchDto {

    private Point departurePoint;

    private Point arrivalPoint;

    private LocalDate departureDate;
}
