package by.bsuir.lab1.dto;

import by.bsuir.lab1.entity.Point;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class RouteDto {

    private Long id;

    private LocalTime departureTime;

    private Point departurePoint;

    private LocalTime arrivalTime;

    private Point arrivalPoint;

    private LocalDate departureDate;

    private LocalDate arrivalDate;
}
