package by.bsuir.lab1.controller;

import by.bsuir.lab1.dto.PassengerDto;
import by.bsuir.lab1.dto.RouteDto;
import by.bsuir.lab1.dto.SearchDto;
import by.bsuir.lab1.dto.UserDto;
import by.bsuir.lab1.entity.Point;
import by.bsuir.lab1.entity.Route;
import by.bsuir.lab1.service.PassengerService;
import by.bsuir.lab1.service.PointService;
import by.bsuir.lab1.service.RouteService;
import by.bsuir.lab1.service.UserService;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/api/v1/routes",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@Api(value = "Controller for routes")
public class RouteController {

    private final RouteService routeService;
    private final PassengerService passengerService;
    private final PointService pointService;

    @PostMapping("/{routeId}")
    public PassengerDto addPassenger(@PathVariable(value = "routeId") Long routeId,
                                     @RequestBody UserDto userDto) {
        return passengerService.create(routeId, userDto.getId());
    }

    @GetMapping(value = "/points", consumes = MediaType.ALL_VALUE)
    public Set<Point> getPoints() {
        return pointService.getAll();
    }

    @PostMapping("/search")
    public Set<RouteDto> searchRoute(@RequestBody SearchDto searchDto) {
        return routeService.search(searchDto);
    }
}
