package by.bsuir.lab1.controller;

import by.bsuir.lab1.dto.UserDto;
import by.bsuir.lab1.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/api/v1/users",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@Validated
@Api(value = "Controller for users")
public class UserController {

    private final UserService userService;

    @ApiOperation(value = "Add a new user")
    @PostMapping
    public UserDto registerUser(@RequestBody UserDto userDto) {
        return userService.create(userDto);
    }
}
