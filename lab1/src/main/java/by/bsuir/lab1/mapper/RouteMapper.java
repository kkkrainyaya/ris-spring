package by.bsuir.lab1.mapper;

import by.bsuir.lab1.dto.RouteDto;
import by.bsuir.lab1.entity.Route;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RouteMapper {

    RouteDto toDto(Route route);

    Route fromDto(RouteDto routeDto);
}
