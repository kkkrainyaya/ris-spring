package by.bsuir.lab1.mapper;

import by.bsuir.lab1.dto.UserDto;
import by.bsuir.lab1.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    UserDto toDto(User user);

    User fromDto(UserDto userDto);
}
