package by.bsuir.lab1.mapper;

import by.bsuir.lab1.dto.PassengerDto;
import by.bsuir.lab1.entity.Passenger;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PassengerMapper {

    @Mapping(target = "user.id", source = "userDto.id")
    @Mapping(target = "route.id", source = "routeDto.id")
    Passenger fromDto(PassengerDto passengerDto);

    @Mapping(target = "userDto.id", source = "user.id")
    @Mapping(target = "routeDto.id", source = "route.id")
    PassengerDto toDto(Passenger passenger);
}
