package by.bsuir.lab1.mapper;

import by.bsuir.lab1.dto.PointDto;
import by.bsuir.lab1.entity.Point;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PointMapper {

    Point fromDto(PointDto pointDto);

    PointDto toDto(Point point);
}
