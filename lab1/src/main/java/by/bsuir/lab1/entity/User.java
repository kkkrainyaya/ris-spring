package by.bsuir.lab1.entity;

import lombok.Data;

import javax.persistence.*;

@Table(name = "users")
@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String surname;

    private String name;

    private String patronymic;

    private String phone;

    private String passportNumber;
}
