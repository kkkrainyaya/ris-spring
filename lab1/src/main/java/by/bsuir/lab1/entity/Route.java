package by.bsuir.lab1.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Table(name = "route")
@Data
@Entity
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalTime departureTime;

    private LocalDate departureDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "departure_point_id")
    private Point departurePoint;

    private LocalTime arrivalTime;

    private LocalDate arrivalDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "arrival_point_id")
    private Point arrivalPoint;

    private Integer numOfPlaces;
}
