package by.bsuir.lab1.entity;

import lombok.Data;

import javax.persistence.*;

@Table(name = "point")
@Data
@Entity
public class Point {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
}
