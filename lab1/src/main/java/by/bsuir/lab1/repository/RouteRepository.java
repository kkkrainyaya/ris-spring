package by.bsuir.lab1.repository;

import by.bsuir.lab1.entity.Point;
import by.bsuir.lab1.entity.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Set;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {

    Set<Route> findAllByArrivalPointAndDeparturePointAndDepartureDate(Point arrivalPoint, Point departurePoint, LocalDate date);
}
