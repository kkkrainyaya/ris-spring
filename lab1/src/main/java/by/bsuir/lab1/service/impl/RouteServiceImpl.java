package by.bsuir.lab1.service.impl;

import by.bsuir.lab1.dto.RouteDto;
import by.bsuir.lab1.dto.SearchDto;
import by.bsuir.lab1.mapper.RouteMapper;
import by.bsuir.lab1.repository.RouteRepository;
import by.bsuir.lab1.service.RouteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class RouteServiceImpl implements RouteService {

    private final RouteRepository routeRepository;
    private final RouteMapper routeMapper;

    @Override
    public Set<RouteDto> getAll() {
        return routeRepository.findAll()
                .stream()
                .map(routeMapper::toDto)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<RouteDto> search(SearchDto searchDto) {
        log.info("Search: {}", searchDto);
        val result = routeRepository.findAllByArrivalPointAndDeparturePointAndDepartureDate(
                searchDto.getArrivalPoint(), searchDto.getDeparturePoint(), searchDto.getDepartureDate())
                .stream()
                .map(routeMapper::toDto)
                .collect(Collectors.toSet());
        log.info("Routes: {}", result);
        return result;
    }
}
