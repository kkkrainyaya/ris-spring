package by.bsuir.lab1.service.impl;

import by.bsuir.lab1.dto.PassengerDto;
import by.bsuir.lab1.entity.Passenger;
import by.bsuir.lab1.entity.Route;
import by.bsuir.lab1.entity.User;
import by.bsuir.lab1.mapper.PassengerMapper;
import by.bsuir.lab1.repository.PassengerRepository;
import by.bsuir.lab1.repository.RouteRepository;
import by.bsuir.lab1.repository.UserRepository;
import by.bsuir.lab1.service.PassengerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
public class PassengerServiceImpl implements PassengerService {

    private final PassengerRepository passengerRepository;
    private final RouteRepository routeRepository;
    private final UserRepository userRepository;
    private final PassengerMapper passengerMapper;

    @Transactional
    @Override
    public PassengerDto create(Long routeId, Long userId) {
        val passenger = new Passenger();
        passenger.setRoute(routeRepository.getOne(routeId));
        passenger.setUser(userRepository.getOne(userId));
        val saved = passengerRepository.save(passenger);
        log.info("passenger {}", saved);
        return passengerMapper.toDto(saved);
    }
}
