package by.bsuir.lab1.service;

import by.bsuir.lab1.entity.Point;

import java.util.Set;

public interface PointService {

    Set<Point> getAll();
}
