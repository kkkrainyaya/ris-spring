package by.bsuir.lab1.service;

import by.bsuir.lab1.dto.RouteDto;
import by.bsuir.lab1.dto.SearchDto;

import java.util.Set;

public interface RouteService {

    Set<RouteDto> getAll();

    Set<RouteDto> search(SearchDto searchDto);
}
