package by.bsuir.lab1.service;

import by.bsuir.lab1.dto.UserDto;

public interface UserService {

    /**
     * Create new user
     *
     * @param user - new user
     * @return - created user
     */
    UserDto create(UserDto user);
}
