package by.bsuir.lab1.service;

import by.bsuir.lab1.dto.PassengerDto;

public interface PassengerService {

    PassengerDto create(Long routeId, Long userId);
}
