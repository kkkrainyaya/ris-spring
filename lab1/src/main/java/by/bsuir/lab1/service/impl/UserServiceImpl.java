package by.bsuir.lab1.service.impl;

import by.bsuir.lab1.dto.UserDto;
import by.bsuir.lab1.mapper.UserMapper;
import by.bsuir.lab1.repository.UserRepository;
import by.bsuir.lab1.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Transactional
    @Override
    public UserDto create(UserDto userDto) {
        val user = userMapper.fromDto(userDto);
        val savedUser = userRepository.save(user);
        return userMapper.toDto(savedUser);
    }
}
