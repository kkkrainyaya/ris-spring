package by.bsuir.lab1.service.impl;

import by.bsuir.lab1.entity.Point;
import by.bsuir.lab1.repository.PointRepository;
import by.bsuir.lab1.service.PointService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class PointServiceImpl implements PointService {

    private final PointRepository pointRepository;

    @Override
    public Set<Point> getAll() {
        return new HashSet<>(pointRepository.findAll());
    }
}
