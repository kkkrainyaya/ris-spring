import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Search} from "../components/entities/search";
import {User} from "../components/entities/user";
import {stringify} from "querystring";

const API_URL = 'http://localhost:8080/api/v1';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class RouteService {


  constructor(private http: HttpClient) {
  }

  getAllPoints(): Observable<any> {
    return this.http.get(API_URL + '/routes/points', httpOptions);
  }

  searchRoutes(route: Search): Observable<any> {
    console.log(route);
    return this.http.post(API_URL + '/routes/search', JSON.stringify(route), httpOptions)
  }

  addUser(user: User): Observable<any> {
    return this.http.post(API_URL + '/users', user, httpOptions);
  }

  addPassenger(route: any, user: User): Observable<any> {
    return this.http.post(API_URL + `/routes/${route.id}`, JSON.stringify(user), httpOptions)
  }
}

