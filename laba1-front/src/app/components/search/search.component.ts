import {Component, OnInit} from '@angular/core';
import {RouteService} from "../../services/route.service";
import {Point} from "../entities/point";
import {Search} from "../entities/search";
import {User} from "../entities/user";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  points: Point[] = [];
  selectedRoutes: any;
  show = false;
  booked = false;

  route: Search = new Search();
  user: User = new User()
  selected: any;

  constructor(private service: RouteService) {
    service.getAllPoints().subscribe(data => this.points = data);
    this.route.arrivalPoint = new Point();
    this.route.departurePoint = new Point();
  }

  ngOnInit(): void {
  }

  getRoutes() {
    this.service.searchRoutes(this.route)
      .subscribe(data => {
        this.selectedRoutes = data;
        console.log(this.selectedRoutes)
      });
  }

  book(route: any) {
    this.show = true;
    this.selected = route;
  }

  addPassenger() {
    this.service.addUser(this.user).subscribe(data => {
      this.user = data;
      this.service.addPassenger(this.selected, this.user).subscribe(data =>
        this.booked = true
        )
    })
  }
}
