export class User {
  id: number;
  surname: string;
  name: string;
  patronymic: string;
  passportNumber: string;
  phone: string;
}
